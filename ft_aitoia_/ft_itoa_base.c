/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msorin <msorin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/27 11:26:47 by msorin            #+#    #+#             */
/*   Updated: 2019/06/27 11:28:39 by msorin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_itoa_div_U(char **str, size_t nb, size_t ind)
{
	if (ind)
		ft_itoa_div_U(str, nb / 16, ind - 1);
	(*str)[ind] = D_HEX_UC[nb % 16];
}

static void	ft_itoa_div(char **str, size_t nb, size_t base, size_t ind)
{
	if (ind)
		ft_itoa_div(str, nb / base, base, ind - 1);
	(*str)[ind] = D_HEX_LC[nb % base];
}

static size_t	nb_len(int nb, size_t base)
{
	int		ret;

	ret = 1;
	while ((nb /= base))
		ret++;
	return (ret);
}

static char	get_base(size_t base)
{
	if (base == 2)
		return ('b');
	if (base == 8)
		return ('o');
	if (base == 16)
		return ('x');
	return ('0');
}

char		*ft_itoa_base(int nb, size_t base, size_t mlen, int flag)
{
	char	*ret;
	size_t	nlen;
	size_t	tmp;

	nlen = nb_len(nb, base);
	nlen = ft_max(nlen, mlen);
	if (base == 10)
		nlen += nb < 0;
	else if (flag)
		nlen += 2;
	if (!(ret = ft_strinit('0', nlen)))
		return (NULL);
	tmp = (base == 10 && nb < 0) ? 0 - nb : nb;
	if (base == 16 && flag & 2)
		ft_itoa_div_U(&ret, nb, nlen - 1);
	else
		ft_itoa_div(&ret, nb, base, nlen - 1);
	if (base == 10 && nb < 0)
		*ret = '-';
	if (flag)
		ret[1] = get_base(base);
	return (ret);
}
