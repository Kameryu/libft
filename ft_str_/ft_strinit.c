/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strinit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msorin <msorin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:11:01 by msorin            #+#    #+#             */
/*   Updated: 2019/07/22 11:11:02 by msorin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strinit(char c, size_t size)
{
	char	*ret;
	size_t	i;

	if (!(ret = ft_strnew(size)))
		return (NULL);
	i = 0;
	while (i < size)
		ret[i++] = c;
	return (ret);
}
