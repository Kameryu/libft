/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msorin <msorin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:06:39 by msorin            #+#    #+#             */
/*   Updated: 2019/07/22 11:06:53 by msorin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <stdlib.h>

int		ft_error(char *to_print, int to_free)
{
	ft_putstr_fd(to_print, 2);
	if (to_free)
		free(to_print);
	return (1);
}
